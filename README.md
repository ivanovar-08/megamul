### MEGAMUL
is a PHP package allowing to show some MUL files on the web as PNG images
generated "on fly".

Special thanks: Timur aka **HotRide** for sharing several code snippets
and general help on MUL internal file format.